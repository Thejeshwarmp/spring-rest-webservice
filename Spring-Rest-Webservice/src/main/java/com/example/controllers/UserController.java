package com.example.controllers;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.request.UserDetailsRequestModel;
import com.example.model.response.UserDetailResponse;
import com.example.service.UserService;
import com.example.shared.dto.UserDto;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserService userService;
	
	@GetMapping
	public UserDetailsRequestModel getUser() {
		return new UserDetailsRequestModel();
	}
	@PostMapping
	public UserDetailResponse createUser(@RequestBody UserDetailsRequestModel UserrequestModel) {
	
		UserDetailResponse returnValue = new UserDetailResponse();
		UserDto dto = new UserDto();
		BeanUtils.copyProperties(UserrequestModel, dto);
		UserDto createdUser = userService.createUser(dto);
		BeanUtils.copyProperties(createdUser,returnValue);
		return returnValue;
	}
	
}
