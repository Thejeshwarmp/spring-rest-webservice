package com.example.service.impl;

import java.util.ArrayList;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.io.entity.UserEntity;
import com.example.repository.UserRepository;
import com.example.service.UserService;
import com.example.shared.dto.UserDto;
import com.example.utils.AppUtils;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepo;
	
	@Autowired
    private AppUtils utils;
	
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Override
	public UserDto createUser(UserDto user) {
		
		if(userRepo.findByEmail(user.getEmail())!=null) throw new RuntimeException("Already Exits");
		UserEntity entity = new UserEntity();
		BeanUtils.copyProperties(user, entity);
		String publicUserId = utils.generateUserId(5);
		entity.setUserId(publicUserId);
		entity.setEncryptedPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		UserEntity userEntity = userRepo.save(entity);
		UserDto userDto = new UserDto();
		BeanUtils.copyProperties(userEntity, userDto);

		return userDto;
	}

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		
		UserEntity entity = userRepo.findByEmail(email);
		if(entity == null) throw new UsernameNotFoundException(email);
		return new User(entity.getEmail(),entity.getEncryptedPassword(),new ArrayList<>());
	}

}
